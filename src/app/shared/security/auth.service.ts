import { User } from './../model/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';
import { Injectable } from '@angular/core';
import { FirebaseAuth } from 'angularfire2';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { AuthInfo } from './authInfo';
import { Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authInfo$: Observable<AuthInfo>;

  private userSub: Subscription;
  private user$: Subject<User>;

  constructor(private auth: AngularFireAuth) {
    this.user$ = new Subject<User>();
   }

  
  login(email, password): Observable <any> {
    return Observable.fromPromise(
      this.auth.auth.signInWithEmailAndPassword(email, password)
    )
  }

  signup(email, password): Observable <any> {
    return Observable.fromPromise(
      this.auth.auth.createUserWithEmailAndPassword(email, password)
    )
  }

  isUserLogged(): boolean {
    console.log("**user object: " + this.auth.auth.currentUser);
    if (this.auth.auth.currentUser) {
        return true;
    } else {
        return false;
    }
}

  
}
