import { Lesson } from './lesson';
import { AngularFireDatabase } from 'angularfire2/database';
import { Course } from './course';
import { Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/mergeMap';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private af: AngularFireDatabase) {

  }

  findAllCourses(): Observable<Course[]> {
    return this.af.list<Course>('courses').valueChanges()
     // .do(console.log)
      .map(Course.fromJsonArray);
  }

 /* findCourseByUrl(courseUrl:string): Observable<Course> {
    return this.af.list<Course>('courses', ref => ref.orderByChild('url')
      .equalTo(courseUrl)).valueChanges()
      .map(results => results[0]);
  }

  findLessonKeysPerCourseUrl(courseUrl: string): Observable<string[]> {
    return this.findCourseByUrl(courseUrl)
      .switchMap(course => this.af.list(`lessonsPerCourse/${course.$key}`).valueChanges())
      .do(console.log)
      .map(lspc => lspc.map(lpc => lpc.$key));
  }

  findAllLessonsForCourse(courseUrl: string): Observable<Lesson[]> {
   // const course$ = this.findCourseByUrl(courseUrl);

    return this.findLessonKeysPerCourseUrl(courseUrl)
      .map(lspc => lspc.map(lessonKey => this.af.object('lessons/' + lessonKey)))
      .flatMap(fbojs => Observable.combineLatest(fbojs));

  }*/

  findLessonsForCourse(courseUrl: string): Observable<Lesson[]> {
    const course$ = this.af.list<Course>('courses', ref => ref.orderByChild('url')
      .equalTo(courseUrl)).valueChanges().do(console.log);

    course$.subscribe();

    return Observable.of([]);
  }

} 
