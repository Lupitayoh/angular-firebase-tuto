import { AngularFireDatabase } from 'angularfire2/database';
import { Lesson } from './lesson';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LessonsService {

  constructor(private af: AngularFireDatabase) { }

  findAllLessons(): Observable<Lesson[]> {
    return this.af.list<Lesson>('lessons').valueChanges()
      .do(console.log)
      .map(Lesson.fromJsonList);
  }
}
