
export class User {
    constructor(
        public $key:string,
        public email:string,
        public password: string
    ){}

    static fromJson({$key, email, password}) {
        return new User($key, email, password);
    }

    static fromJsonArray(json : any[]) : User[] {
        return json.map(User.fromJson);
    }

}