import { firebaseConfig } from './../environments/firebase.config';


import { Component } from '@angular/core';
import { initializeApp, database} from 'firebase';
import { Observable } from 'rxjs/Observable';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-firebase-app';

  constructor(private db: AngularFireDatabase) {
    /*const courses$: FirebaseListObservable<any> = db.list('courses');
    
    const course$ = db.object('courses/-Kk9Mmh3iKishK2jY9Z0');
    course$.subscribe(console.log);
    }*/
  }
}
